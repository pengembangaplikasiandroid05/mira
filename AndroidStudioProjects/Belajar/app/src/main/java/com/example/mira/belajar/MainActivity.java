package com.example.mira.belajar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText nemail;
    private EditText npassword;
    private Button nlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // INISIALISASI VARIABEL
        nemail = (EditText) findViewById(R.id.email);
        npassword = (EditText) findViewById(R.id.password);
        nlogin = (Button) findViewById(R.id.login);

        // INI ADLAAH FUNGSI UNTUK BUTTON LOGIN
        nlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (nemail.getText().toString().equals("mira_manis@gmail.com") && npassword.getText().toString().equals("miramanis"))
                {
                    // INTENS ANTAR ACTIVITY
                    Intent i = new Intent(MainActivity.this,kamera.class);
                    startActivity(i);
                }
            }
        });
    }


}
